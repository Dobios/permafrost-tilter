Permafrost Tilter  
================  
**************
# Installation  
This repo contains the source code of the game with no project files. To compile it yourself, please create a gradle or idea project using the provided source. Otherwise, you can directly play the game by following the instructions below.

# Playing the game  
If you're only interested in playing the game, Permafrost_tilter.zip contains an executable named `PermafrostTilter.jar`. Once unzipped, please excecute the `.jar` file from the same folder (in the same directory as the `res` folder).  
You must have a java version >= 1.8 to be able to run the game.  

# Guide  
A small guide has been written and was added under README.pdf. It contains a small description of the game mechanics and controls as well as the layouts of each level.  

# Development 
Note that the source code was last updated in March of 2017 and contains a non-finished version of a procedural level at the end. If you feel like working on the project, I highly recommend using Java 8 to avoid having any compatibility issues. 
